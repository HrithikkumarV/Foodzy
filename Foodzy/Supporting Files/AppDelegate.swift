//
//  AppDelegate.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 27/04/22.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

   
    private var windowController : NSWindowController!
    private var window : NSWindow!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
//        openMainWindow()
        window = NSWindow(contentRect: .zero, styleMask: [.resizable,.closable,.miniaturizable,.titled], backing: .buffered, defer: false)
        window.contentViewController = SplitViewAssignmentViewController()
        window.minSize = NSSize(width: 1000, height: 600)
        window.center()
        window.makeKeyAndOrderFront(nil)
        window.title = "Split View"
        UserDefaults.standard.set(CGFloat(200), forKey: "NSInitialToolTipDelay")
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }

    
    private func openMainWindow(){
        windowController = MainWindowController()
        windowController.window?.contentViewController = NavigationController(rootViewController: MainViewController())
        windowController.window?.makeKeyAndOrderFront(nil)
        windowController.window?.makeMain()
    }
   

}


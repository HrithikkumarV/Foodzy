//
//  APICallerManager.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 26/05/22.
//

import Foundation


class APICallerManager{
    
    static let shared  = APICallerManager()
    
    private init(){
    }
    func getSecretCode(completion: @escaping (_ code : String?) -> Void){
        var datatask : URLSessionDataTask?
        datatask?.cancel()
        var secretCode  = [String : String]()
        let url = URL(string: "https://api.jsonbin.io/V3/b/61b9cb56229b23312cdbbfed/latest")
                var request = URLRequest(url: url!)
                let header = ["X-Master-Key": "$2b$10$OL9YNafdeNmHeq4c.c0Tme55ivkiFkhUFCxBzC9dXQkjmYZb9Pebu",
                              "X-Bin-Meta": "false"]
                request.allHTTPHeaderFields = header
                request.httpMethod = "GET"
         datatask = URLSession.shared.dataTask(with: request) { (data,response, error) in
            if error == nil && data != nil {
                do {
                    secretCode  = try (JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : String] ?? ["":""])
                    DispatchQueue.main.async {
                        completion(secretCode["secretCode"])

                    }
                }
                catch{
                    print("Error parsing json")
                }
            }
        }
        datatask?.resume()
    }
    
    func getDetailsOfPincode( pincode : String ,complition: @escaping (_ pincodeDetails : PincodeDetails) -> Void){
        var pincodeDetails : PincodeDetails = PincodeDetails()
        let url = URL(string: "https://api.postalpincode.in/pincode/" + pincode)
        var request = URLRequest(url: url!)

        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { (data,response, error) in
            if error == nil && data != nil {
                do {
                    let postDetails  = try (JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[String : Any]])
                    
                    if postDetails?[0]["Status"] as! String != "Error" {
                        for postDetail in postDetails?[0]["PostOffice"] as? NSArray ?? []{
                            let post = postDetail as? Dictionary<String, Any>
                            pincodeDetails.country =  post?["Country"]  as? String ?? ""
                            pincodeDetails.state =  post?["State"] as? String ?? ""
                            pincodeDetails.city =  post?["Division"] as? String ?? ""
                            pincodeDetails.pincode =  post?["Pincode"] as? String ?? ""
                            pincodeDetails.localities.append(post?["Name"] as? String ?? "")
                        }
                    }
                    DispatchQueue.main.async {
                        complition(pincodeDetails)
                    }
                    
                }
                catch{
                    print("Error parsing json")
                }
            }
        }.resume()
        
    }
    
    func getCouponCode(couponCode : String, completion: @escaping (_ couponCode : String? ,_ discountPercentage : Int , _ maximumDiscountAmount : Int) -> Void){
        var datatask : URLSessionDataTask?
        datatask?.cancel()
        let url = URL(string: "https://api.jsonbin.io/V3/b/622dd537a703bb67492a37e9/latest")
                var request = URLRequest(url: url!)
                let header = ["X-Master-Key": "$2b$10$OL9YNafdeNmHeq4c.c0Tme55ivkiFkhUFCxBzC9dXQkjmYZb9Pebu",
                              "X-Bin-Meta": "false"]
                request.allHTTPHeaderFields = header
                request.httpMethod = "GET"
        datatask = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let dataResponse = data,
                  error == nil else {
                  print(error?.localizedDescription ?? "Response Error")
                  return }
            do{
                _ = try JSONSerialization.jsonObject(with:
                                       dataResponse, options: [])
                let decoder = JSONDecoder()
                let model = try decoder.decode([CouponCode].self, from:
                             dataResponse)
                var couponDetails : CouponCode = CouponCode(couponCode: "", discountPercentage: 0, maximumDiscountAmount: 0)
                for coupons in model{
                    if(coupons.couponCode == couponCode){
                        couponDetails = coupons
                        break
                    }
                }
                DispatchQueue.main.async {
                    completion(couponDetails.couponCode, couponDetails.discountPercentage, couponDetails.maximumDiscountAmount)
                }
                
                print(model[0].couponCode)
             } catch let parsingError {
                print("Error", parsingError)
           }
        }
        datatask?.resume()
        
    }
}

struct CouponCode : Codable{
    let couponCode : String
    let discountPercentage , maximumDiscountAmount  : Int    
}

//
//  ViewController.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 27/04/22.
//

import Cocoa

class TableViewController: NSViewController, SearchSuggestionTableSectionHeaderViewDelegate{
    
    var isSecure : Bool = false
    
    var theme : Theme = Theme()
   
    private let searchTypes : [SearchType] = [.searchHistoryItem , .searchSuggestionItem]
   
    
    private let searchItems : [SearchType : [SearchModel]] = [.searchHistoryItem : [SearchModel(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , SearchModel(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),SearchModel(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),SearchModel(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , SearchModel(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),SearchModel(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),SearchModel(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , SearchModel(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),SearchModel(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),SearchModel(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , SearchModel(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),SearchModel(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),SearchModel(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , SearchModel(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),SearchModel(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),SearchModel(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , SearchModel(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),SearchModel(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem)] , .searchSuggestionItem : [SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),SearchModel(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , SearchModel(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),SearchModel(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem)]]
    

    private var dataForTable : [(sectionType : TableViewSectionType, data : Any)] = []
    
    private let cachedTableViewCells = (SearchSuggestionTableSectionHeaderViewCell : SearchSuggestionTableSectionHeaderViewCell(), SearchSuggestionAndSearchHistoryTableViewCell : SearchSuggestionAndSearchHistoryTableViewCell())
    
    private let pasteBoard = NSPasteboard.PasteboardType(rawValue: "SearchFood.search")

    
    
    private let searchBar : NSSearchField = {
        let searchBar = NSSearchField()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.wantsLayer = true
        searchBar.focusRingType = .none
        return searchBar
        
    }()
    
    
    

    private let tableView : NSTableView = {
        let tableView = NSTableView()
        tableView.intercellSpacing = NSSize(width: 0, height: 2)
        tableView.selectionHighlightStyle = .regular
        tableView.style = .plain
        tableView.allowsColumnResizing = true
        tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        return tableView
    }()
    
    private var tableHeight : CGFloat = 0
   
    
    private let tableColumn1 : NSTableColumn = {
        let tableColumn = NSTableColumn()

        return tableColumn
    }()

    
    private let scrollView : NSScrollView = {
        let scrollView = NSScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
  
    
    private let visualEffect : NSVisualEffectView = {
        let visualEffect = NSVisualEffectView()
        visualEffect.blendingMode = NSVisualEffectView.BlendingMode.behindWindow
        visualEffect.material = NSVisualEffectView.Material.underWindowBackground
        visualEffect.translatesAutoresizingMaskIntoConstraints = false
        return visualEffect
    }()
    

    
   
    
   
    override func loadView() {
        self.view = NSView()
        addVisualEffect()
        addSearchBar()
        addScrollView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        prepareDataForTable()
        addTableViewProperties()
        addScrollViewProperties()
        addObservers()
       
    }
    
    
    override func swipe(with event: NSEvent) {
        super.swipe(with: event)
        
//        let x = event.deltaX
//        let y = event.deltaY
//            if (x != 0) {
//                swipeColorValue = (x > 0)  ? SwipeLeftGreen : SwipeRightBlue;
//            }
//            if (y != 0) {
//                swipeColorValue = (y > 0)  ? SwipeUpRed : SwipeDownYellow;
//            }
//        let swipeEvent : NSEvent.SwipeTrackingOptions
//            switch (swipeColorValue) {
//                case SwipeLeftGreen:
//                    direction = @"left";
//                    break;
//                case SwipeRightBlue:
//                    direction = @"right";
//                    break;
//                case SwipeUpRed:
//                    direction = @"up";
//                    break;
//                case SwipeDownYellow:
//                default:
//                    direction = @"down";
//                    break;
//            }
//            [resultsField setStringValue:[NSString stringWithFormat:@"Swipe %@", direction]];
//            [self setNeedsDisplay:YES];
//        let parent = self.parent
//        parent?.transition(from: self, to: (parent?.children.first)!, options: .slideLeft, completionHandler: nil)
    }
    
    
    
   
    
    
    override func viewDidAppear() {
        super.viewDidAppear()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
   
   
    

 
    override func viewWillLayout() {
        super.viewWillLayout()
        
    }
   
    
    
    
    override func viewDidLayout() {
        super.viewDidLayout()
        print("Search Bar Frame  : " ,searchBar.frame ,
               "\nSearch Bar Bounds : " ,searchBar.bounds)

        
    }

    
    

    
    
    func addScrollViewProperties(){
        scrollView.documentView = tableView
        scrollView.contentView.postsBoundsChangedNotifications
        
    }
    
    
    func addTableViewProperties(){
        tableView.headerView = nil
        tableView.registerForDraggedTypes([pasteBoard])
        tableView.addTableColumn(tableColumn1)
        addSearchSuggestionTableSectionHeaderViewCellAsTableViewSubview()
        addSearchSuggestionAndSearchHistoryTableViewCellAsTableViewSubview()
       
    }
    
    private func prepareDataForTable(){
        var index = 0
        for searchType in searchTypes{
            dataForTable.append((sectionType: .sectionHeader, data: searchType))
            for searchItem in searchItems[searchType] ?? []{
                index += 1
                dataForTable.append((sectionType: .sectionRow, data: searchItem))
            }
            index += 1
        }
       
    }
    
    private func addObservers(){
        addinterfaceModeObserver()
        addScrollObserver()
    }
    
    private func addinterfaceModeObserver(){
        DistributedNotificationCenter.default.addObserver(self, selector: #selector(interfaceModeChanged(sender:)), name: NSNotification.Name(rawValue: "AppleInterfaceThemeChangedNotification"), object: nil)
    }
    
    private func addScrollObserver(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(scrollViewScrolls),
                                               name: NSView.boundsDidChangeNotification,
                                               object: scrollView.contentView)
    }
    
    private func applyTheme(){
        searchBar.layer?.backgroundColor = theme.searchBarBackgroundColor
        searchBar.textColor = theme.searchBarTextColor
        searchBar.font = theme.searchBarFont
        tableView.backgroundColor = theme.tableViewBackgroundColor
        scrollView.backgroundColor = theme.scrollViewBackgroundColor
        view.layer?.backgroundColor = theme.viewBackgroundColor
    }

    private func addScrollView() {
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([scrollView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    
    }
    
    private func addVisualEffect() {
        view.addSubview(visualEffect, positioned: .below, relativeTo: nil )
        NSLayoutConstraint.activate([visualEffect.topAnchor.constraint(equalTo: view.topAnchor),visualEffect.leftAnchor.constraint(equalTo: view.leftAnchor),visualEffect.rightAnchor.constraint(equalTo: view.rightAnchor),visualEffect.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    
    }
    
    
    private func addSearchBar(){
        view.addSubview(searchBar)
        
        NSLayoutConstraint.activate([searchBar.topAnchor.constraint(equalTo: view.topAnchor),searchBar.rightAnchor.constraint(equalTo: view.rightAnchor),searchBar.leftAnchor.constraint(equalTo: view.leftAnchor),searchBar.heightAnchor.constraint(equalToConstant: 50)])
        
    }
    
    private func addSearchSuggestionTableSectionHeaderViewCellAsTableViewSubview(){
        tableView.addSubview(cachedTableViewCells.SearchSuggestionTableSectionHeaderViewCell)
        cachedTableViewCells.SearchSuggestionTableSectionHeaderViewCell.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([cachedTableViewCells.SearchSuggestionTableSectionHeaderViewCell.topAnchor.constraint(equalTo: tableView.topAnchor),cachedTableViewCells.SearchSuggestionTableSectionHeaderViewCell.leftAnchor.constraint(equalTo: tableView.leftAnchor),cachedTableViewCells.SearchSuggestionTableSectionHeaderViewCell.rightAnchor.constraint(equalTo: tableView.rightAnchor)])
        
    }
    
    private func addSearchSuggestionAndSearchHistoryTableViewCellAsTableViewSubview(){
        tableView.addSubview(cachedTableViewCells.SearchSuggestionAndSearchHistoryTableViewCell)
        cachedTableViewCells.SearchSuggestionAndSearchHistoryTableViewCell.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([cachedTableViewCells.SearchSuggestionAndSearchHistoryTableViewCell.topAnchor.constraint(equalTo: tableView.topAnchor),cachedTableViewCells.SearchSuggestionAndSearchHistoryTableViewCell.leftAnchor.constraint(equalTo: tableView.leftAnchor),cachedTableViewCells.SearchSuggestionAndSearchHistoryTableViewCell.rightAnchor.constraint(equalTo: tableView.rightAnchor)])
        
    }
    
    
    func didTapClearHistory(sender : NSButton) {
//        let searchType = dataForTable[sender.tag].data as! SearchType
//        var index = 0
//        while index < dataForTable.count {
//            if let search = dataForTable[index].data as? SearchModel{
//                if search.searchType == searchType {
//                    dataForTable.remove(at: index)
//                    index -= 1
//                }
//
//            }
//            index += 1
//
//        }
//        tableView.reloadData()
//        self.navigationController?.popViewController(animated: true)
        
        isSecure.toggle()
        
    }
    

    @objc func interfaceModeChanged(sender: NSNotification) {
        print("Hello")
    }
    
    @objc func scrollViewScrolls(){
//        print(tableHeight,tableView.frame.height)
   }
    
    
    struct Theme{
        var searchBarBackgroundColor : CGColor  = .clear
        var searchBarTextColor : NSColor = .white
        var searchBarFont : NSFont = NSFont.systemFont(ofSize: 20, weight: .medium)
        var scrollViewBackgroundColor : NSColor = .controlBackgroundColor
        var tableViewBackgroundColor : NSColor = .clear
        var viewBackgroundColor : CGColor = .clear
        
    }
    

}

extension TableViewController: NSTableViewDataSource {
    
   
  func numberOfRows(in tableView: NSTableView) -> Int {
      return dataForTable.count
  }
   
  func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        let pasteboardItem = NSPasteboardItem()
        if(dataForTable[row].sectionType != .sectionHeader){
            pasteboardItem.setString("\(row)", forType: pasteBoard)
        }
        return pasteboardItem
  }

    
  func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
      
      guard
          let item = info.draggingPasteboard.pasteboardItems?.first,
          let originalRow = Int(item.string(forType: pasteBoard)!)
          else { return [] }
      
      if dataForTable[originalRow].sectionType == .sectionRow &&
            dataForTable[row].sectionType == .sectionRow{
          let searchModelOfOriginalRow = dataForTable[originalRow].data as! SearchModel
          let searchModelOfNewRow = dataForTable[row].data as! SearchModel
          if dropOperation == .on && searchModelOfOriginalRow.searchType == searchModelOfNewRow.searchType{
                    return .move
                } else {
                    return []
                }
      }
      
      return []
    }
    
    
    
    
    
    
       
  func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
            guard
                let item = info.draggingPasteboard.pasteboardItems?.first,
                let originalRow = Int(item.string(forType: pasteBoard)!)
                else { return false }
            let newRow = row
            print(originalRow,newRow)
            print(dataForTable[originalRow],dataForTable[newRow])
            let temp = dataForTable[originalRow]
            dataForTable.remove(at: originalRow)
            dataForTable.insert(temp, at: newRow)
            tableView.beginUpdates()
            tableView.moveRow(at: originalRow, to: newRow)
            tableView.endUpdates()
            return true
    }
    
    
   
}

extension TableViewController: NSTableViewDelegate {
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat{
        let dataForRow =  dataForTable[row]
        if(dataForRow.sectionType == .sectionHeader){
            let searchType = dataForRow.data as! SearchType
            let searchSuggestionTableHeaderView = cachedTableViewCells.SearchSuggestionTableSectionHeaderViewCell
            searchSuggestionTableHeaderView.configureData(searchType: searchType.rawValue,index: row)
            searchSuggestionTableHeaderView.needsLayout = true
            searchSuggestionTableHeaderView.layout()
            searchSuggestionTableHeaderView.layoutSubtreeIfNeeded()
            print(searchSuggestionTableHeaderView.contentView.bounds.height)
            tableHeight += searchSuggestionTableHeaderView.contentView.bounds.height + tableView.intercellSpacing.height
            return searchSuggestionTableHeaderView.contentView.bounds.height
        }
        else{
            let searchModel = dataForRow.data as! SearchModel
            let search = cachedTableViewCells.SearchSuggestionAndSearchHistoryTableViewCell
            search.configureContent(seacrhSuggesionCellContents: searchModel)
            search.needsLayout = true
            search.layout()
            search.layoutSubtreeIfNeeded()
            print(search.contentView.bounds.height)
            tableHeight += search.contentView.bounds.height + tableView.intercellSpacing.height
            return search.contentView.bounds.height
        }
        
        
        
    }
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        if(dataForTable[row].sectionType == .sectionHeader){
            return false
        }
        else{
            return true
        }
    }
    
  
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        print("selected : " ,  tableView.selectedRow)
        
    }
   
    
    
  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
      
      
      let dataForRow =  dataForTable[row]
      

      if(dataForRow.sectionType == .sectionHeader){
          let searchType = dataForRow.data as! SearchType
          if let searchSuggestionTableHeaderView = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(SearchSuggestionTableSectionHeaderViewCell.cellIdentifier), owner: self) as? SearchSuggestionTableSectionHeaderViewCell{
              searchSuggestionTableHeaderView.configureData(searchType: searchType.rawValue,index: row)
              searchSuggestionTableHeaderView.delegate = self
              return searchSuggestionTableHeaderView
          }
          else{
              let searchSuggestionTableHeaderView = SearchSuggestionTableSectionHeaderViewCell()
              searchSuggestionTableHeaderView.identifier = NSUserInterfaceItemIdentifier(SearchSuggestionTableSectionHeaderViewCell.cellIdentifier)
              searchSuggestionTableHeaderView.configureData(searchType: searchType.rawValue,index: row)
              searchSuggestionTableHeaderView.delegate = self
              
              return searchSuggestionTableHeaderView
          }
      }
      else{
          let searchModel = dataForRow.data as! SearchModel
          if let search =  tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier), owner: self) as? SearchSuggestionAndSearchHistoryTableViewCell{
              search.configureContent(seacrhSuggesionCellContents: searchModel)
              return search
          }
          else{
              let search = SearchSuggestionAndSearchHistoryTableViewCell()
              search.identifier = NSUserInterfaceItemIdentifier(SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier)
              search.configureContent(seacrhSuggesionCellContents: searchModel)
              return search
          }
         
         
          
          
      }
      
      

  }
    
    func tableView(_ tableView: NSTableView, rowViewForRow row: Int) -> NSTableRowView? {
        if(dataForTable[row].sectionType == .sectionHeader){
            return NSTableRowView()
        }
        return SearchSuggestionTableRowView()
    }
    
    
  

    
   
    
    func tableView(_ tableView: NSTableView, rowActionsForRow row: Int, edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        if(dataForTable[row].sectionType == .sectionRow ){
            if edge == .trailing {
                let deleteAction = NSTableViewRowAction(style: .destructive, title: "Delete", handler: {
                    [weak self] (nil, row)  in
                    self?.dataForTable.remove(at: row)
                    tableView.removeRows(at: IndexSet(integer: row), withAnimation: .effectFade)
                })
                deleteAction.backgroundColor = NSColor.red
               
                    deleteAction.image = NSImage(systemSymbolName: "xmark.bin.fill", accessibilityDescription: "Delete")?.withSymbolConfiguration(.init(scale: .large))?.image(withTintColor: .white.withAlphaComponent(0.9))
                
                return [deleteAction]
            } else {
                let addToSearchBarAction = NSTableViewRowAction(style: .regular, title: "Add To Search Bar", handler: {[weak self]_,_ in
                    let  dataForRow = self?.dataForTable[row].data as! SearchModel
                    self?.searchBar.stringValue = dataForRow.searchItemName
                })
                addToSearchBarAction.backgroundColor = NSColor.systemBlue
               
                    addToSearchBarAction.image = NSImage(systemSymbolName: "arrow.up", accessibilityDescription: "select To Top")?.withSymbolConfiguration(.init(scale: .large))?.image(withTintColor: .white.withAlphaComponent(0.9))
                self.navigationController?.pushViewController(SplitViewController(), animated: true)
                
                return [addToSearchBarAction]
            }
            
           
        }
        else{
            return []
        }
    }
    
    func tableViewColumnDidResize(_ notification: Notification) {
        tableView.floatsGroupRows = false
        tableHeight = 0
            let allIndexes = IndexSet(integersIn: 0..<tableView.numberOfRows)
            tableView.noteHeightOfRows(withIndexesChanged: allIndexes)
        print(tableHeight,tableView.frame.height)
        tableView.floatsGroupRows = true
        tableView.frame.size.height = tableHeight
    }
    
    
    
    
    
    func tableView(_ tableView: NSTableView, isGroupRow row: Int) -> Bool {
        if(dataForTable[row].sectionType == .sectionHeader){
            return true
        }
        else{
            return false
        }
    }

    
    
    
}

enum TableViewSectionType {
    case sectionHeader
    case sectionRow
}



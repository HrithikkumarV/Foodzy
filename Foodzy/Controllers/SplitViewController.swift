//
//  splitViewController.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 18/05/22.
//


import AppKit

class SplitViewController : NSSplitViewController, MainViewControllerDelegate{
    var flag : Bool = true
    var Welcomewindow : NSWindow!
    func didTapRestaurantButton() {
        if(flag){
            
            sidebarWithViewController.animator().isCollapsed = true
            
            Welcomewindow = NSWindow(contentRect: .zero, styleMask: [.resizable,.closable,.titled , .miniaturizable], backing: .buffered, defer: false)
            Welcomewindow.contentViewController = sidebarWithViewController.viewController
                    Welcomewindow.center()
                    Welcomewindow.isMovable = true
            Welcomewindow.maxSize = NSSize(width: 800, height: 800)
            Welcomewindow.minSize = NSSize(width: 300, height: 300)
                    let controller = NSWindowController(window: Welcomewindow)
                    controller.showWindow(nil)
            Welcomewindow.collectionBehavior = .fullScreenNone
            flag = false
        }
        else{
            Welcomewindow.close()
            sidebarWithViewController.animator().isCollapsed = false
   
            flag = true
        }
                
        
    }
    
    
    
   
    lazy var sidebarWithViewController : NSSplitViewItem = {
        let mainViewController = MainViewController()
        mainViewController.delegate = self
        let sideBar = NSSplitViewItem(sidebarWithViewController: mainViewController)
        sideBar.minimumThickness = 300
        sideBar.maximumThickness = 300
//        sideBar.preferredThicknessFraction = 0.2
        sideBar.collapseBehavior = .useConstraints
        sideBar.canCollapse = false
//        sideBar.collapseBehavior = .preferResizingSplitViewWithFixedSiblings
        return sideBar
    }()
    
    var contentListWithViewController : NSSplitViewItem = {
        let tableViewController = TableViewController()
        tableViewController.view.frame.size = NSSize(width: 200, height: 500)
        let contentList = NSSplitViewItem(contentListWithViewController: NavigationController(rootViewController:tableViewController))
//        contentList.maximumThickness = 350
//        contentList.minimumThickness = 300
        contentList.preferredThicknessFraction = 0.4
        return contentList
    }()
    
    var detailViewController : NSSplitViewItem = {
        let detailView = NSSplitViewItem(viewController: MainViewController())
        detailView.preferredThicknessFraction = 0.4
        return detailView
    }()
    
    var contentListWithViewController1 : NSSplitViewItem = {
        let tableViewController = TableViewController()
        tableViewController.view.frame.size = NSSize(width: 200, height: 500)
        let contentList = NSSplitViewItem(contentListWithViewController: NavigationController(rootViewController:tableViewController))
//        contentList.maximumThickness = 350
//        contentList.minimumThickness = 300
        contentList.preferredThicknessFraction = 0.4
        return contentList
    }()
    
    var detailViewController1 : NSSplitViewItem = {
        let detailView = NSSplitViewItem(viewController: MainViewController())
        detailView.preferredThicknessFraction = 0.4
        return detailView
    }()
    
    var contentListWithViewController2 : NSSplitViewItem = {
        let tableViewController = TableViewController()
        tableViewController.view.frame.size = NSSize(width: 200, height: 500)
        let contentList = NSSplitViewItem(contentListWithViewController: NavigationController(rootViewController:tableViewController))
//        contentList.maximumThickness = 350
//        contentList.minimumThickness = 300
        contentList.preferredThicknessFraction = 0.4
        return contentList
    }()
    
    var detailViewController2 : NSSplitViewItem = {
        let detailView = NSSplitViewItem(viewController: MainViewController())
        detailView.preferredThicknessFraction = 0.4
        return detailView
    }()
    
    var contentListWithViewController3 : NSSplitViewItem = {
        let tableViewController = TableViewController()
        tableViewController.view.frame.size = NSSize(width: 200, height: 500)
        let contentList = NSSplitViewItem(contentListWithViewController: NavigationController(rootViewController:tableViewController))
//        contentList.maximumThickness = 350
//        contentList.minimumThickness = 300
        contentList.preferredThicknessFraction = 0.4
        return contentList
    }()
    
    var detailViewController3 : NSSplitViewItem = {
        let detailView = NSSplitViewItem(viewController: MainViewController())
        detailView.preferredThicknessFraction = 0.4
        return detailView
    }()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitView.dividerStyle = .thin
        addSplitViewItems()
        sidebarWithViewController.viewController.navigationController  = self.navigationController
        
        
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
       
    }
    
    
    override func splitViewWillResizeSubviews(_ notification: Notification) {
        if(splitView.frame.size.width > 1000){
            detailViewController.isCollapsed = false
        }
        else{
            detailViewController.isCollapsed = true
        }
    }
    
   
    override func splitView(_ splitView: NSSplitView, shouldHideDividerAt dividerIndex: Int) -> Bool {
       
            return true
        
    }
    
    
   
    
    
    func addSplitViewItems(){
        addSplitViewItem(sidebarWithViewController)
        addSplitViewItem(detailViewController)
        addSplitViewItem(contentListWithViewController)
//        addSplitViewItem(contentListWithViewController1)
//        addSplitViewItem(detailViewController1)
//        addSplitViewItem(contentListWithViewController2)
//        addSplitViewItem(detailViewController2)
//        addSplitViewItem(contentListWithViewController3)
//        addSplitViewItem(detailViewController3)
        
    }
    
    
    override func splitView(_ splitView: NSSplitView, effectiveRect proposedEffectiveRect: NSRect, forDrawnRect drawnRect: NSRect, ofDividerAt dividerIndex: Int) -> NSRect {

//        if dividerIndex == 1 {
//            return NSRect()
//        }
//        return super.splitView(splitView, effectiveRect: proposedEffectiveRect, forDrawnRect: drawnRect, ofDividerAt: dividerIndex)
        return NSRect()
    }
    
    
    
   
}



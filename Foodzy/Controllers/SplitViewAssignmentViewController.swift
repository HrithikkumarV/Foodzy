//
//  SplitViewControllerAssignment.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 29/05/22.
//

import Cocoa

class SplitViewAssignmentViewController :  NSSplitViewController{
    
    let sv1 : NSSplitViewItem = {
        let vc = DummyVCForSplitViewController()
        vc.theme.backgroundColour = NSColor.systemGray.cgColor
        let sv1  = NSSplitViewItem(sidebarWithViewController: vc)
        sv1.minimumThickness = 300
        sv1.maximumThickness = 500
        sv1.canCollapse = false
        return sv1
    }()
    
    let sv2 : NSSplitViewItem = {
        let vc = ContentSplitViewOfAssignmentViewController()
        let sv2  = NSSplitViewItem(viewController: vc)
        sv2.minimumThickness = 300
        return sv2
    }()
    
    let sv3 : NSSplitViewItem = {
        let vc = DummyVCForSplitViewController()
        vc.theme.backgroundColour = NSColor.systemBlue.cgColor
        let sv3  = NSSplitViewItem(sidebarWithViewController: vc)
        sv3.minimumThickness = 300
        return sv3
    }()
    
    


    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSplitViewItem(sv1)
        addSplitViewItem(sv2)
        addSplitViewItem(sv3)
    }
}


class ContentSplitViewOfAssignmentViewController :  NSSplitViewController{
    
    let sv1 : NSSplitViewItem = {
        let vc = DummyVCForSplitViewController()
        vc.theme.backgroundColour = NSColor.systemPink.cgColor
        let sv1  = NSSplitViewItem(sidebarWithViewController: vc)
        sv1.minimumThickness = 300
        sv1.canCollapse = false
        return sv1
    }()
    
    let sv2 : NSSplitViewItem = {
        let vc = DummyVCForSplitViewController()
        vc.theme.backgroundColour = NSColor.systemRed.cgColor
        let sv2  = NSSplitViewItem(viewController: vc)
        sv2.minimumThickness = 300
        return sv2
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitView.isVertical = false
        addSplitViewItem(sv1)
        addSplitViewItem(sv2)
    }
}


class DummyVCForSplitViewController : NSViewController{
    var theme : Theme = Theme()
    
    override func loadView() {
        view = NSView()
        view.wantsLayer = true
        view.layer?.backgroundColor = theme.backgroundColour
    }
    
    struct Theme{
        var backgroundColour : CGColor = .white
    }
}

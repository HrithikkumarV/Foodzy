//
//  File.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 19/05/22.
//

import AppKit

class MainViewController : NSViewController, MainViewDelegate{
    
    weak var delegate : MainViewControllerDelegate?
    
    
    private let contentView : NSView = {
        let contentView = NSView()
        contentView.frame.size = NSSize(width: 500, height: 500)
        return contentView
    }()
    
    
    private let visualEffect : NSVisualEffectView = {
        let visualEffect = NSVisualEffectView()
        visualEffect.blendingMode = NSVisualEffectView.BlendingMode.behindWindow
        visualEffect.material = NSVisualEffectView.Material.underWindowBackground
        visualEffect.translatesAutoresizingMaskIntoConstraints = false
        return visualEffect
    }()
    
    private let mainView : MainView = {
        let mainView = MainView()
        mainView.layer?.cornerRadius = 10
        mainView.translatesAutoresizingMaskIntoConstraints = false
        return mainView
    }()
    
    
    override func loadView() {
        view = contentView
        addVisualEffect()
        addMainView()
        mainView.delegate = self
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    
    
    private func addVisualEffect() {
        contentView.addSubview(visualEffect, positioned: .below, relativeTo: nil )
        NSLayoutConstraint.activate([visualEffect.topAnchor.constraint(equalTo: contentView.topAnchor),visualEffect.leftAnchor.constraint(equalTo: contentView.leftAnchor),visualEffect.rightAnchor.constraint(equalTo: contentView.rightAnchor),visualEffect.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)])
    
    }
    
 
  
    
    private func addMainView() {
        contentView.addSubview(mainView)
        NSLayoutConstraint.activate([mainView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 5),mainView.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 5),mainView.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -5),mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -5)])
    
    }
    
    func didTapUserButton() {
//        self.view.window?.addChildWindow(MainWindow(contentRect: NSRect(x: 0, y: 0, width: 800, height: 800), styleMask: [.titled,.miniaturizable,.closable,.resizable], backing: .buffered, defer: false), ordered: .above)
//        let Welcomewindow = MainWindow(contentRect: NSRect(x: 0, y: 0, width: 0, height: 0), styleMask: [.resizable,.closable,.titled , .miniaturizable], backing: .buffered, defer: false)
//        Welcomewindow.contentViewController = TableViewController()
//        Welcomewindow.center()
//        Welcomewindow.isMovable = true
//
//    
//        let controller = NSWindowController(window: Welcomewindow)
//        controller.showWindow(nil)
//        addChild(TableViewController())
//        addChild(SplitViewController())
//
//        self.transition(from: TableViewController(), to: SplitViewController())
        self.navigationController?.pushViewController(TableViewController(), animated: true)
    }
    
    func didTapRestaurantButton() {
//        self.view.window?.addChildWindow(MainWindowController().window!, ordered: .above)
        
//        present(TableViewController(), asPopoverRelativeTo: NSRect(x: 0, y: 0, width: 800, height: 800), of: mainView, preferredEdge: .minX, behavior: .semitransient)
        
//        presentAsModalWindow(TableViewController())
        
//        present(TableViewController(), animator: ReplacePresentationAnimator() )
        
        
//        self.navigationController?.pushViewController(TableViewController(), animated: true)
//        self.navigationController?.popViewController(animated: true)
        delegate?.didTapRestaurantButton()
    }
}



protocol MainViewControllerDelegate : AnyObject{
    func didTapRestaurantButton()
}

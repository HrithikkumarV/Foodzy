//
//  PageController.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 19/05/22.
//

import AppKit

class PageViewController : NSPageController, NSPageControllerDelegate{
    
    private var pages : [String] = ["page1" ,"page2" , "page3" , "page4" , "page5"]
    
    override func loadView() {
        view = NSView(frame: NSRect(x: 0, y: 0, width: 800, height: 800))
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrangedObjects = pages
        delegate = self
        transitionStyle = .stackHistory
    }
    
  
    
    
    func pageController(_ pageController: NSPageController, viewControllerForIdentifier identifier: String) -> NSViewController {

            switch identifier {
            case "page1":
                return TableViewController()
            case "page2":
                return TableViewController()
            case "page3":
                return TableViewController()
            case "page4":
                return TableViewController()
            case "page5":
                return TableViewController()
            default:
                return TableViewController()
            }
        }

        func pageController(_ pageController: NSPageController, identifierFor object: Any) -> String {
            return String(describing: object)

        }

        func pageControllerDidEndLiveTransition(_ pageController: NSPageController) {
            self.completeTransition()
        }
    
    
}



//
//  MainWindow.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 19/05/22.
//

import Cocoa

class MainWindowController : NSWindowController, NSWindowDelegate{
    init(){
        super.init(window: MainWindow(contentRect: .zero, styleMask: [.closable , .miniaturizable , .titled,.resizable], backing: .buffered, defer: false))
        self.window?.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func windowWillClose(_ notification: Notification) {
        let alert = NSAlert()
        alert.messageText = "message Text"
        alert.informativeText = "informative Text"
        alert.showsSuppressionButton = true
        alert.addButton(withTitle: "Ok")
        alert.addButton(withTitle: "Cancel")
        alert.buttons.first?.bezelColor = .red
        alert.alertStyle = .warning
       
        if alert.runModal() == .alertFirstButtonReturn {
            return
        }
        else{
            cancelOperation(nil)
        }
    }
   
}

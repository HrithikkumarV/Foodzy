//
//  SearchSuggessionModel.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 09/05/22.
//

import Foundation

struct SearchModel {
    var searchItemName : String = ""
    var searchItemType : SearchItemType = .Search
    var searchType : SearchType = .searchSuggestionItem
}

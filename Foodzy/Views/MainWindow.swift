//
//  MainWindow.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 19/05/22.
//

import Cocoa


class MainWindow : NSWindow{
   
    override init(contentRect: NSRect, styleMask style: NSWindow.StyleMask, backing backingStoreType: NSWindow.BackingStoreType, defer flag: Bool) {
        super.init(contentRect: contentRect, styleMask: style, backing: backingStoreType, defer: flag)
        self.title = "Foodzy"
        self.minSize = NSSize(width: 500, height: 500)
        self.center()
//        self.titlebarAppearsTransparent = true
    }
    
    
}

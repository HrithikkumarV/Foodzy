//
//  SearchSuggestionTableCellView.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 06/05/22.
//

import Cocoa

enum SearchType : String{
    case searchHistoryItem = " Search History Item "
    case searchSuggestionItem = " Search suggestion Item"
}

enum SearchItemType : String{
    case Menu
    case Restaurant
    case Search
}



class SearchSuggestionAndSearchHistoryTableViewCell : NSTableCellView, NSTextFieldDelegate {
    
    
    static let cellIdentifier = "SearchSuggestionAndSearchHistoryTableViewCell"
    
    var theme : Theme = Theme()
    
    private let padding : CGFloat = 10
    
    
    private let searchTypeCellImageView : NSImageView = {
        let  searchTypeCellImageView = NSImageView()
        searchTypeCellImageView.translatesAutoresizingMaskIntoConstraints = false
        searchTypeCellImageView.wantsLayer = true
        searchTypeCellImageView.imageAlignment = .alignCenter
        searchTypeCellImageView.imageScaling = .scaleAxesIndependently
        
        return searchTypeCellImageView
    }()
    
    let searchSuggestionAndHistoryNameCellLabel : NSTextField = {
        let  searchSuggestionAndHistoryNameCellLabel = NSTextField()
        searchSuggestionAndHistoryNameCellLabel.wantsLayer = true
        searchSuggestionAndHistoryNameCellLabel.isEditable = false
        searchSuggestionAndHistoryNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        searchSuggestionAndHistoryNameCellLabel.alignment = .left
        searchSuggestionAndHistoryNameCellLabel.isBordered = false
        searchSuggestionAndHistoryNameCellLabel.isBezeled = false
        searchSuggestionAndHistoryNameCellLabel.isSelectable = true
        searchSuggestionAndHistoryNameCellLabel.sizeToFit()
        searchSuggestionAndHistoryNameCellLabel.lineBreakMode = .byWordWrapping
        searchSuggestionAndHistoryNameCellLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        searchSuggestionAndHistoryNameCellLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return searchSuggestionAndHistoryNameCellLabel
    }()
    
    private  let searchItemTypeCellLabel : NSTextField = {
        let searchItemTypeCellLabel = NSTextField()
        searchItemTypeCellLabel.translatesAutoresizingMaskIntoConstraints = false
        searchItemTypeCellLabel.alignment = .left
        searchItemTypeCellLabel.isEditable = false
        searchItemTypeCellLabel.wantsLayer = true
        searchItemTypeCellLabel.isBordered = false
        searchItemTypeCellLabel.isBezeled = false
        searchItemTypeCellLabel.sizeToFit()
        searchItemTypeCellLabel.lineBreakMode = .byWordWrapping
        searchItemTypeCellLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        searchItemTypeCellLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return searchItemTypeCellLabel
    }()

    let contentView : NSView = {
        let  contentView = NSView()
        contentView.wantsLayer = true
        contentView.layer?.cornerRadius = 10
        contentView.translatesAutoresizingMaskIntoConstraints = false
        return contentView
    }()
    
    init(){
        super.init(frame: .zero)
        initialiseViewElements()
        applyTheme()
    }
   
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        searchSuggestionAndHistoryNameCellLabel.stringValue = ""
        searchItemTypeCellLabel.stringValue = ""
        searchTypeCellImageView.image = nil
    }
    
   
    
    func configureContent(seacrhSuggesionCellContents : SearchModel){
        searchSuggestionAndHistoryNameCellLabel.stringValue = "There's often a lot of confusion, but if you're looking for a general answer to the question, “How many sentences in a paragraph?” the answer is there are 3 to 8 sentences in a paragraph. The important key to take away from this answer is that it's a rule-of-thumb."
//        seacrhSuggesionCellContents.searchItemName
        searchItemTypeCellLabel.stringValue = "What is an example of a paragraph? A good example of a paragraph contains a topic sentence, details and a conclusion. 'There are many different kinds of animals that live in China. Tigers and leopards are animals that live in China's forests in the north."
//        seacrhSuggesionCellContents.searchItemType.rawValue
        
        searchSuggestionAndHistoryNameCellLabel.delegate  = self
        updateSearchTypeCellImageView(searchType: seacrhSuggesionCellContents.searchType)
        
    }
    
    
    
    
    func applyTheme(){
        contentView.layer?.backgroundColor = theme.contentViewBackgroundColor
        searchTypeCellImageView.layer?.backgroundColor = theme.searchTypeCellImageViewBackgroundColor
        searchSuggestionAndHistoryNameCellLabel.backgroundColor = theme.searchSuggestionAndHistoryNameCellLabelBackgroundColor
        searchSuggestionAndHistoryNameCellLabel.textColor = theme.searchSuggestionAndHistoryNameCellLabelTextColor
        searchSuggestionAndHistoryNameCellLabel.font = theme.searchSuggestionAndHistoryNameCellLabelfont
        searchItemTypeCellLabel.textColor = theme.searchItemTypeCellLabelTextColor
        searchItemTypeCellLabel.backgroundColor = theme.searchItemTypeCellLabelBackgroundColor
    }
    
    
   

   
    
   
    private func initialiseViewElements(){
        addContentView()
        addSearchTypeCellImageView()
        addSearchSuggestionAndHistoryNameCellLabel()
        addSearchItemTypeCellLabel()
    }
    
    
    private func updateSearchTypeCellImageView(searchType : SearchType){
       
        if(searchType == .searchHistoryItem){
            searchTypeCellImageView.image = NSImage(systemSymbolName: "clock", accessibilityDescription: "searched")?.image(withTintColor: .gray)

        }
        else{
            searchTypeCellImageView.image = NSImage(systemSymbolName: "magnifyingglass", accessibilityDescription: "Search")?.image(withTintColor: .gray)
        }
        
        
    }
   
    
    private func addContentView(){
        self.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.topAnchor),
            contentView.leftAnchor.constraint(equalTo: self.leftAnchor),
            contentView.rightAnchor.constraint(equalTo:self.rightAnchor),
            ])
    }
    
    
    private func addSearchSuggestionAndHistoryNameCellLabel(){
        contentView.addSubview(searchSuggestionAndHistoryNameCellLabel)
        NSLayoutConstraint.activate([searchSuggestionAndHistoryNameCellLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: padding),
                                     searchSuggestionAndHistoryNameCellLabel.leftAnchor.constraint(equalTo: searchTypeCellImageView.rightAnchor,constant: padding),
                                     searchSuggestionAndHistoryNameCellLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor ,constant: -padding),
                              ])
       
    }
    
    private func addSearchTypeCellImageView(){
        contentView.addSubview(searchTypeCellImageView)
        NSLayoutConstraint.activate([
            searchTypeCellImageView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: padding),
            searchTypeCellImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: padding),
            searchTypeCellImageView.widthAnchor.constraint(equalToConstant: 25),
            searchTypeCellImageView.heightAnchor.constraint(equalToConstant: 25)])
        
        
    }
    
    private func addSearchItemTypeCellLabel(){
        contentView.addSubview(searchItemTypeCellLabel)
        NSLayoutConstraint.activate([
            searchItemTypeCellLabel.topAnchor.constraint(equalTo: searchSuggestionAndHistoryNameCellLabel.bottomAnchor,constant: padding),
            searchItemTypeCellLabel.leftAnchor.constraint(equalTo: searchTypeCellImageView.rightAnchor,constant: padding),
            searchItemTypeCellLabel.rightAnchor.constraint(equalTo:contentView.rightAnchor,constant: -padding),
            contentView.bottomAnchor.constraint(equalTo: searchItemTypeCellLabel.bottomAnchor,constant: 10)
            
        ])
        
        
    }
    
    struct Theme {
        var contentViewBackgroundColor : CGColor = .clear
        var searchTypeCellImageViewBackgroundColor : CGColor = .clear
        var searchSuggestionAndHistoryNameCellLabelBackgroundColor : NSColor = .clear
        var searchSuggestionAndHistoryNameCellLabelTextColor : NSColor = .white
        var searchSuggestionAndHistoryNameCellLabelfont : NSFont = NSFont.systemFont(ofSize: 20, weight: .medium)
        var searchItemTypeCellLabelTextColor : NSColor = .systemGray
        var searchItemTypeCellLabelBackgroundColor : NSColor = .clear
    }
    
}

class SearchSuggestionTableSectionHeaderViewCell : NSTableCellView{
    
    weak var delegate : SearchSuggestionTableSectionHeaderViewDelegate?
    
    static let cellIdentifier = "SearchSuggestionTableSectionHeaderViewCell"
    
    var theme : Theme = Theme()
    
    private let padding : CGFloat = 10
    
    private let searchTypeLabel : SecureTextField = {
        let searchTypeLabel = SecureTextField()
        searchTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        searchTypeLabel.alignment = .left
        searchTypeLabel.isEditable = true
        searchTypeLabel.becomeFirstResponder()
        searchTypeLabel.isSelectable = true
        searchTypeLabel.isBordered = false
        searchTypeLabel.bezelStyle = .roundedBezel
        searchTypeLabel.focusRingType = .none
        searchTypeLabel.lineBreakMode = .byWordWrapping
        searchTypeLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        searchTypeLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return searchTypeLabel
    }()
    
    private lazy var  clearHistoryButton  : NSButton = {
        let clearHistoryButton = NSButton()
        clearHistoryButton.wantsLayer = true
        clearHistoryButton.layer?.cornerRadius = 10
        clearHistoryButton.layer?.borderWidth = 1
        clearHistoryButton.attributedTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "Clear"))
        clearHistoryButton.alignment = .center
        clearHistoryButton.translatesAutoresizingMaskIntoConstraints = false
        clearHistoryButton.target = self
        clearHistoryButton.action = #selector(didTapClearHistory)
        return clearHistoryButton
    }()
    
    let contentView : NSView = {
        let  searchSuggestionTableSectionHeaderViewContentView = NSView()
        searchSuggestionTableSectionHeaderViewContentView.wantsLayer = true
        searchSuggestionTableSectionHeaderViewContentView.layer?.cornerRadius = 10
        searchSuggestionTableSectionHeaderViewContentView.translatesAutoresizingMaskIntoConstraints = false
        return searchSuggestionTableSectionHeaderViewContentView
    }()
    
    init() {
        super.init(frame: .zero)
        
        initialiseViewElements()
        applyTheme()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configureData(searchType : String , index : Int){
//        searchTypeLabel.stringValue = searchType
//        "A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument."
        clearHistoryButton.tag = index
    }
    
    
    
    func applyTheme(){
        contentView.layer?.backgroundColor = theme.contentViewBackgroundColor
        searchTypeLabel.backgroundColor = theme.searchTypeLabelBackgroundColor
        searchTypeLabel.textColor = theme.searchTypeLabelTextColor
        searchTypeLabel.font = theme.searchTypeLabelFont
        clearHistoryButton.layer?.backgroundColor = theme.clearHistoryButtonBackgroundColor
        clearHistoryButton.layer?.borderColor = theme.clearHistoryButtonBorderColor
        clearHistoryButton.setTitleColor(color: theme.clearHistoryButtonTextColor)
        clearHistoryButton.font = theme.clearHistoryButtonFont
    }
    

    
    
    private func initialiseViewElements(){
        addContentView()
        addClearHistoryButtonLabel()
        addSearchTypeLabel()
    }
    
    
    
    private func addContentView(){
        self.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.topAnchor),
            contentView.leftAnchor.constraint(equalTo: self.leftAnchor),
            contentView.rightAnchor.constraint(equalTo:self.rightAnchor),
            ])
        
    }
    
    
    private func addClearHistoryButtonLabel(){
        contentView.addSubview(clearHistoryButton)
        NSLayoutConstraint.activate([clearHistoryButton.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -padding),
                                     clearHistoryButton.topAnchor.constraint(equalTo: contentView.topAnchor,constant: padding),
                                     clearHistoryButton.widthAnchor.constraint(equalToConstant: 80),
                                     clearHistoryButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -padding),
                                 
                 ])
       
    }
    
    private func addSearchTypeLabel(){
        contentView.addSubview(searchTypeLabel)
        NSLayoutConstraint.activate([
            searchTypeLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: padding),
            searchTypeLabel.rightAnchor.constraint(equalTo: clearHistoryButton.leftAnchor,constant: -padding),searchTypeLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: padding),
            contentView.bottomAnchor.constraint(equalTo: searchTypeLabel.bottomAnchor,constant: 10)
])
        
    }
    
    
    
    @objc private func didTapClearHistory(sender: NSButton){
        delegate?.didTapClearHistory(sender : sender)
    }
    
    struct Theme {
        var contentViewBackgroundColor : CGColor = .clear
        var searchTypeLabelBackgroundColor : NSColor = .red
        var searchTypeLabelTextColor : NSColor = .white
        var searchTypeLabelFont : NSFont = NSFont.systemFont(ofSize: 20, weight: .medium)
        var clearHistoryButtonBackgroundColor : CGColor = .clear
        var clearHistoryButtonBorderColor : CGColor = NSColor.lightGray.cgColor
        var clearHistoryButtonTextColor : NSColor = .red
        var clearHistoryButtonFont = NSFont.systemFont(ofSize: 18)
    }
    
    
    
   
}

protocol SearchSuggestionTableSectionHeaderViewDelegate : AnyObject{
    func didTapClearHistory(sender: NSButton)
}



class SearchSuggestionTableRowView : NSTableRowView {
    
    var theme : Theme = Theme()
    
    init(){
        super.init(frame: .zero)
        addTrackingArea(NSTrackingArea(rect: self.frame, options: [.inVisibleRect, .activeAlways, .mouseEnteredAndExited], owner: self, userInfo: nil))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    override func mouseEntered(with event: NSEvent) {
        self.backgroundColor = theme.mouseEnteredHighlightColour
    }
        

    override func mouseExited(with event: NSEvent) {
        self.backgroundColor = .clear
    }
    
   
    
    struct Theme{
        var mouseEnteredHighlightColour : NSColor = .red
    }
  
    
   
}



class SecureTextField : NSTextField{
    var text : String = ""
    
    override func textDidChange(_ notification: Notification) {
        if(text.count < stringValue.count){
            text += String(stringValue.removeLast())
            stringValue += "*"
        }
        else if !text.isEmpty{
            text.removeLast()
        }
        
        print(text)
    }
    
    
}

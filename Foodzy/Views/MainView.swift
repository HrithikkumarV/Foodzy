//
//  MainView.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 19/05/22.
//

import Cocoa

class MainView : NSView{
    
    
    weak var delegate : MainViewDelegate?
    
    var theme : Theme = Theme()
    
    private lazy var userButton : NSButton = {
        let userButton = NSButton()
        userButton.wantsLayer = true
        userButton.layer?.cornerRadius = 20
        userButton.bezelStyle = .shadowlessSquare
        userButton.bezelColor = .clear
        userButton.showsBorderOnlyWhileMouseInside = true
        userButton.attributedTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "USER"))
        userButton.alignment = .center
        userButton.translatesAutoresizingMaskIntoConstraints = false
        userButton.toolTip = "Hello"
        userButton.target = self
        userButton.action = #selector(didTapUserButton)
        
        return userButton
    }()
    
    private lazy var restaurantButton : NSButton = {
        let restaurantButton = NSButton()
        restaurantButton.wantsLayer = true
        restaurantButton.layer?.cornerRadius = 20
        restaurantButton.bezelStyle = .shadowlessSquare
        restaurantButton.bezelColor = .clear
        restaurantButton.showsBorderOnlyWhileMouseInside = true
        restaurantButton.attributedTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "RESTAURANT"))
        restaurantButton.alignment = .center
        restaurantButton.translatesAutoresizingMaskIntoConstraints = false
        restaurantButton.target = self
        restaurantButton.action = #selector(didTapRestaurantButton)
        return restaurantButton
    }()
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(){
        super.init(frame: .zero)
        self.wantsLayer = true
        initialiseViewElements()
        applyTheme()
    }
    
    func applyTheme(){
        self.layer?.backgroundColor = theme.contentViewBackgroundColor
        restaurantButton.layer?.backgroundColor = theme.restaurantButtonBackgroundColor
        
        restaurantButton.setTitleColor(color: theme.restaurantButtonTextColor)
        restaurantButton.font = theme.restaurantButtonFont
        userButton.layer?.backgroundColor = theme.userButtonBackgroundColor
        
        userButton.setTitleColor(color: theme.userButtonTextColor)
        userButton.font = theme.userButtonFont
    }
    
    
    
    
    
    private func initialiseViewElements(){
        addUserButton()
        addRestaurantButton()
    }
    
        
    private func addUserButton(){
        self.addSubview(userButton)
        NSLayoutConstraint.activate([userButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     userButton.bottomAnchor.constraint(equalTo: self.centerYAnchor,constant: -20),
                                     userButton.heightAnchor.constraint(equalToConstant: 100),
                                     userButton.widthAnchor.constraint(equalToConstant: 150)
                                     ])
    
    }
    
    private func addRestaurantButton(){
        self.addSubview(restaurantButton)
        NSLayoutConstraint.activate([
            restaurantButton.centerXAnchor.constraint(equalTo: userButton.centerXAnchor),
            restaurantButton.topAnchor.constraint(equalTo:  userButton.bottomAnchor, constant: 20),
            restaurantButton.heightAnchor.constraint(equalToConstant: 100),
            restaurantButton.widthAnchor.constraint(equalToConstant: 150)
            ])
    }
    
    
    @objc func didTapUserButton(){
        delegate?.didTapUserButton()
    }
    @objc func didTapRestaurantButton(){
        delegate?.didTapRestaurantButton()
    }
    
    struct Theme {
        var contentViewBackgroundColor : CGColor = NSColor.windowBackgroundColor.cgColor
        
        var userButtonBackgroundColor : CGColor = NSColor.systemBlue.cgColor
        
        var userButtonTextColor : NSColor = .white
        var userButtonFont = NSFont.systemFont(ofSize: 20)
        var restaurantButtonBackgroundColor : CGColor = NSColor.systemGreen.cgColor
        var restaurantButtonTextColor : NSColor = .white
        var restaurantButtonFont = NSFont.systemFont(ofSize: 20)
    }
    
    
}
    

    

protocol MainViewDelegate  : AnyObject{
    func didTapUserButton()
    func didTapRestaurantButton()
}

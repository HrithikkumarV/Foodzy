//
//  UserWelcomePageView.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 20/05/22.
//

import Cocoa


class UserWelcomePageView : NSView{
    
    
    
    weak var delegate : UserWelcomePageViewDelegate?
    
    
    
    lazy var setDeliveryAddressButton : NSButton = {
        let setDeliveryAddressButton = NSButton()
        setDeliveryAddressButton.translatesAutoresizingMaskIntoConstraints = false
        setDeliveryAddressButton.attributedTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "Set Delivery Address"))
        setDeliveryAddressButton.font = NSFont.systemFont(ofSize: 16, weight: .regular)
        setDeliveryAddressButton.setTitleColor(color: .white)
        setDeliveryAddressButton.wantsLayer = true
        setDeliveryAddressButton.layer?.backgroundColor = NSColor.systemBlue.cgColor
        setDeliveryAddressButton.layer?.borderWidth = 1
        setDeliveryAddressButton.layer?.borderColor = NSColor.white.cgColor
        setDeliveryAddressButton.layer?.cornerRadius = 5
        setDeliveryAddressButton.target = self
        setDeliveryAddressButton.action = #selector(didTapSetDeliveryAddress)
        setDeliveryAddressButton.bezelStyle = .shadowlessSquare
        setDeliveryAddressButton.bezelColor = .clear
        setDeliveryAddressButton.showsBorderOnlyWhileMouseInside = true
        return setDeliveryAddressButton
    }()
    let haveAnAccountLabel : NSTextField = {
        let haveAnAccountLabel = NSTextField()
        haveAnAccountLabel.translatesAutoresizingMaskIntoConstraints =  false
        haveAnAccountLabel.stringValue = "Have an account ?"
        haveAnAccountLabel.isEditable = false
        haveAnAccountLabel.isSelectable = true
        haveAnAccountLabel.textColor = .systemGray
        haveAnAccountLabel.backgroundColor = .white
        haveAnAccountLabel.sizeToFit()
        haveAnAccountLabel.lineBreakMode = .byWordWrapping
        haveAnAccountLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        haveAnAccountLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        haveAnAccountLabel.font = NSFont.systemFont(ofSize: 14.0, weight: .regular)
        haveAnAccountLabel.isBezeled = false
        haveAnAccountLabel.isBordered = false
        return haveAnAccountLabel
    }()
    lazy var loginButton : NSButton = {
        let loginButton = NSButton()
        loginButton.attributedTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "Login"))
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.setTitleColor(color: .systemBlue)
        loginButton.wantsLayer = true
        loginButton.layer?.backgroundColor = .white
        loginButton.font = NSFont.systemFont(ofSize: 16.0, weight: .regular)
        loginButton.sizeToFit()
        loginButton.bezelStyle = .shadowlessSquare
        loginButton.bezelColor = .clear
        loginButton.showsBorderOnlyWhileMouseInside = true
        loginButton.target = self
        loginButton.action = #selector(didTapLogin)
        return loginButton
    }()
    let readyToOrderFromYourFavouriteRestaurantsLabel : NSTextField = {
        let readyToOrderFromYourFavouriteRestaurantsLabel = NSTextField()
        readyToOrderFromYourFavouriteRestaurantsLabel.translatesAutoresizingMaskIntoConstraints =  false
        readyToOrderFromYourFavouriteRestaurantsLabel.stringValue = "Ready to order from your Favourite Restaurants ?"
        readyToOrderFromYourFavouriteRestaurantsLabel.isEditable = false
        readyToOrderFromYourFavouriteRestaurantsLabel.isSelectable = true
        readyToOrderFromYourFavouriteRestaurantsLabel.textColor = .systemGray
        readyToOrderFromYourFavouriteRestaurantsLabel.backgroundColor = .white
        readyToOrderFromYourFavouriteRestaurantsLabel.sizeToFit()
        readyToOrderFromYourFavouriteRestaurantsLabel.lineBreakMode = .byWordWrapping
        readyToOrderFromYourFavouriteRestaurantsLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        readyToOrderFromYourFavouriteRestaurantsLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        readyToOrderFromYourFavouriteRestaurantsLabel.font = NSFont.systemFont(ofSize: 14.0, weight: .regular)
        readyToOrderFromYourFavouriteRestaurantsLabel.isBezeled = false
        readyToOrderFromYourFavouriteRestaurantsLabel.isBordered = false
        return readyToOrderFromYourFavouriteRestaurantsLabel
    }()
    let appWelcomeMessageLabel : NSTextField = {
        let appWelcomeMessageLabel = NSTextField()
        appWelcomeMessageLabel.stringValue = "Order your favourite foods from a wide range of restaurants in your locality delivered quickly to your doorstep"
//        appWelcomeMessageLabel.font = NSFont(name:"ArialRoundedMTBold", size: 35.0)
        appWelcomeMessageLabel.font = NSFont.systemFont(ofSize: 35, weight: .medium)
        appWelcomeMessageLabel.textColor = .black
        appWelcomeMessageLabel.backgroundColor = .white
        appWelcomeMessageLabel.wantsLayer = true
        appWelcomeMessageLabel.isEditable = false
        appWelcomeMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        appWelcomeMessageLabel.alignment = .center
        appWelcomeMessageLabel.isBordered = false
        appWelcomeMessageLabel.isBezeled = false
        appWelcomeMessageLabel.isSelectable = true
        appWelcomeMessageLabel.sizeToFit()
        appWelcomeMessageLabel.lineBreakMode = .byWordWrapping
        appWelcomeMessageLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        appWelcomeMessageLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return appWelcomeMessageLabel
    }()
    let userWelcomeMessageLabel : NSTextField = {
        let  userWelcomeMessageLabel = NSTextField()
        userWelcomeMessageLabel.translatesAutoresizingMaskIntoConstraints =  false
        userWelcomeMessageLabel.font = NSFont.systemFont(ofSize: 30, weight: .regular)
        userWelcomeMessageLabel.textColor = .darkGray
        userWelcomeMessageLabel.backgroundColor = .white
        userWelcomeMessageLabel.isHidden = true
        userWelcomeMessageLabel.sizeToFit()
        return userWelcomeMessageLabel
    }()
    lazy var changeAccountButton : NSButton = {
        let changeAccountButton = NSButton()
        changeAccountButton.translatesAutoresizingMaskIntoConstraints = false
        changeAccountButton.attributedTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "Change Account"))
        changeAccountButton.font = NSFont.systemFont(ofSize: 16.0, weight: .regular)
        changeAccountButton.setTitleColor(color: .systemBlue)
        changeAccountButton.layer?.backgroundColor = .white
        changeAccountButton.isHidden = true
        changeAccountButton.sizeToFit()
        changeAccountButton.target = self
        changeAccountButton.action = #selector(didTapChangeAccount)
        return changeAccountButton
    }()

    
    init(){
        super.init(frame: .zero)
        self.wantsLayer = true
        self.layer?.backgroundColor = .white
        initialiseViewElements()
    }
    
   
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        addAppWelcomeMessageLabel()
        addUserWelcomeMessageLabel()
        addReadyToOrderFromYourFavouriteRestaurantsLabel()
        addSetDeliveryAddressButton()
        addHaveAnAccountLabel()
        addLoginButton()
        addChangeAccountButton()
    }
    
    
    private func addAppWelcomeMessageLabel(){
        
        self.addSubview(appWelcomeMessageLabel)
        NSLayoutConstraint.activate([appWelcomeMessageLabel.topAnchor.constraint(equalTo: self.topAnchor ,constant: 100 ),
                                     appWelcomeMessageLabel.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                     appWelcomeMessageLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),])
        

    }
    
    private func addSetDeliveryAddressButton(){
        self.addSubview(setDeliveryAddressButton)
        NSLayoutConstraint.activate([setDeliveryAddressButton.topAnchor.constraint(equalTo: readyToOrderFromYourFavouriteRestaurantsLabel.bottomAnchor , constant: 10),
                                     setDeliveryAddressButton.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                     setDeliveryAddressButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                     setDeliveryAddressButton.heightAnchor.constraint(equalToConstant: 50)])
       
    }
    
    private func addReadyToOrderFromYourFavouriteRestaurantsLabel(){
        self.addSubview(readyToOrderFromYourFavouriteRestaurantsLabel)
        NSLayoutConstraint.activate([readyToOrderFromYourFavouriteRestaurantsLabel.topAnchor.constraint(equalTo: appWelcomeMessageLabel.bottomAnchor , constant: 30),
                                     readyToOrderFromYourFavouriteRestaurantsLabel.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                     readyToOrderFromYourFavouriteRestaurantsLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                     readyToOrderFromYourFavouriteRestaurantsLabel.heightAnchor.constraint(equalToConstant: 20)])
        
    }
    
    private func addHaveAnAccountLabel(){
        self.addSubview(haveAnAccountLabel)
        NSLayoutConstraint.activate( [haveAnAccountLabel.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor , constant: 10),
                                      haveAnAccountLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor , constant: -30),
                                      haveAnAccountLabel.heightAnchor.constraint(equalToConstant: 20)])
       
    }
    
    private func addLoginButton(){
        self.addSubview(loginButton)
        NSLayoutConstraint.activate([loginButton.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor , constant: 10),
                                     loginButton.leftAnchor.constraint(equalTo: haveAnAccountLabel.rightAnchor),
                                     loginButton.heightAnchor.constraint(equalToConstant: 20)])
    }
    
    private func addChangeAccountButton(){
        self.addSubview(changeAccountButton)
        NSLayoutConstraint.activate([changeAccountButton.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor , constant: 10),
                                     changeAccountButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     changeAccountButton.heightAnchor.constraint(equalToConstant: 20)])
    }
    
    private func addUserWelcomeMessageLabel(){
        self.addSubview(userWelcomeMessageLabel)
        NSLayoutConstraint.activate([userWelcomeMessageLabel.topAnchor.constraint(equalTo: appWelcomeMessageLabel.bottomAnchor , constant: 10),
                                     userWelcomeMessageLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     userWelcomeMessageLabel.heightAnchor.constraint(equalToConstant: 40)])
        
    }
    @objc func didTapSetDeliveryAddress(){
        delegate?.didTapSetDeliveryAddress()
    }

    @objc func didTapLogin(){
        delegate?.didTapLogin()
    }

    @objc func didTapChangeAccount(){
        delegate?.didTapChangeAccount()
    }
    
}


protocol UserWelcomePageViewDelegate :AnyObject{
     func didTapSetDeliveryAddress()

     func didTapLogin()

     func didTapChangeAccount()
}


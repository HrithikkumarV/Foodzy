//
//  AddressView.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 26/05/22.
//


import Cocoa

//class AddressView : NSView{
//    
//   weak var delegate: AddressViewDelegate?
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    init(){
//        super.init(frame: .zero)
//        self.wantsLayer = true
//        self.layer?.backgroundColor = .white
//        createStackView()
//    }
//    
//   
//    
//    
//    
//    private func createStackView(){
//        self.addSubview(stackView)
//        NSLayoutConstraint.activate(getStackViewConstraints())
//    }
//    
//    
//    private func createTextField() -> NSTextField{
//        let textField = NSTextField()
//        textField.backgroundColor = .white
//        textField.textColor = .black
//        textField.translatesAutoresizingMaskIntoConstraints = false
//        textField.isAutomaticTextCompletionEnabled = true
//        textField.allowsCharacterPickerTouchBarItem = true
//        textField.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        return textField
//    }
//   
//    
//   
//   
//    lazy var doorNoAndBuildingNameAndBuildingNoTextField  : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "Door no,Building name & no"
//        textField.addLabelToTopBorder(labelText: " Door no,Building name & no ", fontSize: 15)
//        return textField
//    }()
//    
//    lazy var streetNameTextfield : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "Street Name"
//        textField.addLabelToTopBorder(labelText: " Street Name ", fontSize: 15)
//        return textField
//    }()
//    
//    lazy var landmarkTextfield : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "Landmark"
//        textField.addLabelToTopBorder(labelText: "Landmark", fontSize: 15)
//        return textField
//    }()
//    
//    lazy var localityButton : NSButton  = {
//        let button = NSButton()
//        button.setTitle("Locality", for: .normal)
//        button.addLabelToTopBorder(labelText: " Locality ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
//        button.hideTopBorderLabelInView()
//        button.contentHorizontalAlignment = .left
//        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
//        button.setTitleColor(UIColor.systemGray3, for: .normal)
//        button.titleLabel?.textAlignment = .left
//        button.backgroundColor = .white
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        button.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
//        button.isUserInteractionEnabled = false
//        button.addTarget(self, action: #selector(didTapLocality), for: .touchUpInside)
//        return button
//    }()
//    
//    lazy var cityTextfield : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "City"
//        textField.isUserInteractionEnabled = false
//        textField.addLabelToTopBorder(labelText: " City ", fontSize: 15)
//        return textField
//    }()
//    
//    lazy var stateTextField : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "State"
//        textField.isUserInteractionEnabled = false
//        textField.addLabelToTopBorder(labelText: " State ", fontSize: 15)
//        return textField
//    }()
//    
//    lazy var countryTextfield : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "Country"
//        textField.isUserInteractionEnabled = false
//        textField.addLabelToTopBorder(labelText: " Country ", fontSize: 15)
//        return textField
//    }()
//    
//    lazy var pincodeTextField : NSTextField = {
//        let textField = createTextField()
//        textField.placeholderString = "Pincode"
//        textField.addLabelToTopBorder(labelText: " Pincode ", fontSize: 15)
//        textField.keyboardType = .numberPad
//        let pincodeKeyBoardDoneBarButton = ToolBarUtils().createBarButton(title: "Done")
//        pincodeKeyBoardDoneBarButton.target = self
//        pincodeKeyBoardDoneBarButton.action = #selector(didTapPincodeKeyboardDoneBarButton)
//        let pincodeKeyBoardToolBar = ToolBarUtils().createKeyboardToolBar()
//        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
//        pincodeKeyBoardToolBar.items = [flexibleSpaceForToolBar,pincodeKeyBoardDoneBarButton]
//        textField.inputAccessoryView = pincodeKeyBoardToolBar
//        return textField
//    }()
//    
//    private lazy  var continueButton : NSButton = {
//        let continueButton = NSButton()
//        continueButton.translatesAutoresizingMaskIntoConstraints = false
//        continueButton.setTitle("Continue", for: .normal)
//        continueButton.setTitleColor(.white, for: .normal)
//        continueButton.backgroundColor = .systemBlue
//        continueButton.layer.cornerRadius = 5
//        continueButton.titleLabel?.adjustsFontSizeToFitWidth = true
//        continueButton.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
//        continueButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        return continueButton
//    }()
//    
//    
//    
//   
//     
//    
//    
//    private lazy var stackView : UIStackView = {
//       let stackView = UIStackView(arrangedSubviews: [pincodeTextField,countryTextfield,stateTextField,cityTextfield,localityButton, doorNoAndBuildingNameAndBuildingNoTextField , streetNameTextfield,landmarkTextfield,continueButton])
//        stackView.axis = .vertical
//        stackView.spacing = 15
//        stackView.distribution = .fillProportionally
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        return stackView
//    }()
//    
//    func getMaxYOfLastObject() -> CGFloat{
//        return stackView.frame.maxY
//    }
//   
//    
//
//    
//    
//    
//    
//    
//    private func getStackViewConstraints() -> [NSLayoutConstraint]{
//        let stackViewConstraints = [stackView.topAnchor.constraint(equalTo:self.safeAreaLayoutGuide.topAnchor,constant: 50),
//                                    stackView.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 40),
//                                    stackView.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: -40),
//                                    ]
//        return stackViewConstraints
//    }
//    
//
//    @objc func didTapContinue(){
//        delegate?.didTapContinue()
//    }
//    @objc func didTapPincodeKeyboardDoneBarButton(){
//        delegate?.didTapPincodeKeyboardDoneBarButton()
//    }
//    @objc func didTapLocality(){
//        delegate?.didTapLocality()
//    }
//    @objc func didTapView(){
//        delegate?.didTapView()
//    }
//}
//
//
//protocol AddressViewDelegate : AnyObject{
//    func didTapContinue()
//    func didTapPincodeKeyboardDoneBarButton()
//    func didTapLocality()
//    func didTapView()
//}

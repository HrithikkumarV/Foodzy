//
//  ButtonUtils.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 11/05/22.
//

import Cocoa

extension NSButton {

    func setTitleColor(color: NSColor) {
        let newAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
        let range = NSRange(location: 0, length: attributedTitle.length)

        newAttributedTitle.addAttributes([
            .foregroundColor: color,
        ], range: range)

        attributedTitle = newAttributedTitle
    }
}

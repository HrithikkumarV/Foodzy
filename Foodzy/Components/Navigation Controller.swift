//
//  Navigation Controller.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 23/05/22.
//


import AppKit

public class NavigationController: NSViewController {
    
    

    var currentViewController : NSViewController!
    
    var isTransitionCompleted : Bool = false
    
   open override func loadView() {
      view = NSView()
      view.wantsLayer = true
   }

   public init(rootViewController: NSViewController) {
      super.init(nibName: nil, bundle: nil)
       currentViewController = rootViewController
       rootViewController.navigationController = self
       addChild(rootViewController)
       view.addSubview(rootViewController.view)
       view.frame = rootViewController.view.frame
   }

    public override func viewWillLayout() {
        super.viewWillLayout()
        if(isTransitionCompleted){
            currentViewController.view.frame = view.frame
        }
    }
    
    public override func viewDidAppear() {
        super.viewDidAppear()
        isTransitionCompleted = true
    }

    
    
    
   public required init?(coder: NSCoder) {
      fatalError()
   }

   


   public func pushViewController(_ viewController: NSViewController, animated: Bool) {
       viewController.navigationController = self
       viewController.view.wantsLayer = true
       addChild(viewController)
       view.addSubview(viewController.view)
       isTransitionCompleted = false
      if animated{
          self.transition(from: currentViewController, to: viewController, options: .slideLeft, completionHandler: { [weak self] in
              self?.isTransitionCompleted = true
             
          })
      } else {
          self.transition(from: currentViewController, to: viewController,options: .allowUserInteraction ,completionHandler: { [weak self] in
              self?.isTransitionCompleted = true
              
          })
      }
       currentViewController = viewController
       
   }


   public func popViewController(animated: Bool) {
      
       if children.count >= 2 {
           let newVC = self.children[children.count-2]
           isTransitionCompleted = false
           if animated{
               self.transition(from: currentViewController, to: newVC, options: .slideRight, completionHandler: { [weak self] in
                   self?.isTransitionCompleted = true
                   self?.currentViewController.removeFromParent()
                   self?.currentViewController.view.removeFromSuperview()
                   self?.navigationController = nil
                   self?.currentViewController = newVC
                   
               })
              
          } else {
              self.transition(from: currentViewController, to: newVC,options: .allowUserInteraction,completionHandler: { [weak self] in
                  self?.isTransitionCompleted = true
                  self?.currentViewController.removeFromParent()
                  self?.currentViewController.view.removeFromSuperview()
                  self?.navigationController = nil
                  self?.currentViewController = newVC
              })
          }
       }

   }

}

extension NSViewController {

    
    private static var storedNavigationControllersForViewController : [String : NavigationController] = [:]
    

   public var navigationController: NavigationController? {
      get {
          let classAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
          return NavigationController.storedNavigationControllersForViewController[classAddress]
      } set {
          let classAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
          
          if newValue == nil {
              NavigationController.storedNavigationControllersForViewController.removeValue(forKey: classAddress)
          }
          else{
              NavigationController.storedNavigationControllersForViewController[classAddress] = newValue
          }
      }
   }
}



